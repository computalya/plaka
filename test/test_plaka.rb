# frozen_string_literal: true

require 'minitest/autorun'
require "test_helper"
require "turkish_support"

class TestPlaka < Minitest::Test
  using TurkishSupport
  def setup
    @plaka_instance = Plaka::Plaka.new
  end

  def test_that_it_has_a_version_number
    refute_nil ::Plaka::VERSION
  end

  def test_show_with_integer_input
    result = @plaka_instance.show(3)
    assert_equal "afyon", result
  end

  def test_show_with_integer
    result = @plaka_instance.show(39)
    assert_equal "kırklareli", result
  end

  def test_show_with_integer_as_string
    result = @plaka_instance.show('35')
    assert_equal "izmir", result
  end

  def test_show_with_string_smallcase
    result = @plaka_instance.show('kilis')
    assert_equal 79, result
  end

  def test_show_with_string_uppercase
    skip
    result = @plaka_instance.show('KİLİS')
    assert_equal 79, result
  end

  def test_show_with_turkish_characters
    result = @plaka_instance.show('ÇAnkırı')
    assert_equal 18, result
  end

  def test_show_with_city_name
    result = @plaka_instance.show('mersin')
    assert_equal 33, result
  end

  def test_show_with_accented_city_name
    result = @plaka_instance.show('içel')
    assert_equal 33, result
  end

  def test_show_with_non_existing_city
    result = @plaka_instance.show('non_existing_city')
    refute result
  end

  def test_show_with_non_existing_integer
    result = @plaka_instance.show(99)
    refute result
  end

  def test_invalid_without_any_input
    result = @plaka_instance.valid?()
    refute result
  end

  def test_invalid_with_zero
    result = @plaka_instance.valid?(0)
    refute result
  end

  def test_valid_with_integer
    result = @plaka_instance.valid?(39)
    assert result
  end

  def test_invalid_without_any_string_input
    result = @plaka_instance.valid?('')
    refute result
  end

  def test_valid_with_integer_as_string
    result = @plaka_instance.valid?('39')
    assert result
  end

  def test_valid_with_string
    result = @plaka_instance.valid?('antalya')
    assert result
  end

  def test_invalid_with_empty_string
    result = @plaka_instance.valid?(' ')
    refute result
  end

  def test_invalid_with_non_existing_city
    skip
    result = @plaka_instance.valid?('NonExistingCity')
    refute result
  end

  def test_exists_with_existing_code
    result = @plaka_instance.exists?('33')
    assert result
  end

  def test_exists_with_non_existing_code
    result = @plaka_instance.exists?('0')
    refute result
  end

  def test_exists_with_existing_city
    result = @plaka_instance.exists?('antalya')
    assert result
  end

  def test_exists_with_existing_city_with_multiple_names
    result = @plaka_instance.exists?('mersin')
    assert result
  end

  def test_exists_with_existing_city_with_alternative_names
    result = @plaka_instance.exists?('içel')
    assert result
  end

  def test_exists_with_non_existing_city_name
    result = @plaka_instance.exists?('NonExistingCity')
    refute result
  end

  def test_exists_with_non_existing_integer
    result = @plaka_instance.exists?(0)
    refute result
  end

  def test_exists_with_non_existing_digit
    result = @plaka_instance.exists?(99)
    refute result
  end

  def test_exists_with_existing_digit
    result = @plaka_instance.exists?(33)
    assert result
  end

  def test_has_many_names_with_multiple_names
    result = @plaka_instance.has_many_names('Afyon')
    assert result
  end

  def test_has_many_names_with_single_name
    result = @plaka_instance.has_many_names('Adana')
    refute result
  end

  def test_has_many_names_with_numeric_code
    result = @plaka_instance.has_many_names(33)
    assert result
  end

  def test_has_many_names_with_another_numeric_code
    result = @plaka_instance.has_many_names(34)
    refute result
  end

  def test_has_many_names_with_numeric_code_string
    result = @plaka_instance.has_many_names('33')
    assert result
  end

  def test_has_many_names_with_another_numeric_code_string
    result = @plaka_instance.has_many_names('34')
    refute result
  end

  def test_has_many_names_with_non_existing_city_name
    result = @plaka_instance.has_many_names('NonExistingCity')
    refute result
  end

  def test_has_many_names_with_zero_as_integer
    result = @plaka_instance.has_many_names(0)
    refute result
  end

  def test_has_many_names_with_zero_as_string
    result = @plaka_instance.has_many_names('0')
    refute result
  end

end
