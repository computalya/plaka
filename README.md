# Plaka

TODO: Delete this and the text below, and describe your gem

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/plaka`. To experiment with that code, run `bin/console` for an interactive prompt.

## Installation

TODO: Replace `UPDATE_WITH_YOUR_GEM_NAME_PRIOR_TO_RELEASE_TO_RUBYGEMS_ORG` with your gem name right after releasing it to RubyGems.org. Please do not do it earlier due to security reasons. Alternatively, replace this section with instructions to install your gem from git if you don't plan to release to RubyGems.org.

Install the gem and add to the application's Gemfile by executing:

    $ bundle add UPDATE_WITH_YOUR_GEM_NAME_PRIOR_TO_RELEASE_TO_RUBYGEMS_ORG

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install UPDATE_WITH_YOUR_GEM_NAME_PRIOR_TO_RELEASE_TO_RUBYGEMS_ORG

## Usage

```ruby
bin/console

plaka = Plaka::Plaka.new
# puts plaka.list
# puts plaka.list_all

puts "plaka.show(39)           => #{plaka.show(39)}"
puts "plaka.show('35')         => #{plaka.show('35')}"
puts "plaka.show('kilis')      => #{plaka.show('kilis')}"
puts "plaka.show('KİLİS')      => #{plaka.show('KİLİS')}"
puts "plaka.show('ÇAnkırı')    => #{plaka.show('ÇAnkırı')}"
puts "plaka.show(33)           => #{plaka.show(33)}"
puts "plaka.show(3)            => #{plaka.show(3)}"
puts "plaka.show('mersin')     => #{plaka.show('mersin')}"
puts "plaka.show('içel')       => #{plaka.show('içel')}"

puts "plaka.show('kamil')      => #{plaka.show('kamil')}"
puts "plaka.show(99)           => #{plaka.show(99)}"

puts "plaka.valid?()           => #{plaka.valid?()}"
puts "plaka.valid?(0)          => #{plaka.valid?(0)}"
puts "plaka.valid?(39)         => #{plaka.valid?(39)}"
puts "plaka.valid?('')         => #{plaka.valid?('')}"
puts "plaka.valid?('39')       => #{plaka.valid?('39')}"
puts "plaka.valid?('antalya')  => #{plaka.valid?('antalya')}"
puts "plaka.valid?(' ')        => #{plaka.valid?(' ')}"
puts "plaka.valid?('kamil')    => #{plaka.valid?(' ')}"

puts "plaka.exists?('33')      => #{plaka.exists?('33')}"
puts "plaka.exists?('0')       => #{plaka.exists?('0')}"
puts "plaka.exists?('antalya') => #{plaka.exists?('antalya')}"
puts "plaka.exists?('mersin')  => #{plaka.exists?('mersin')}"
puts "plaka.exists?('içel')    => #{plaka.exists?('içel')}"
puts "plaka.exists?('isviçre') => #{plaka.exists?('isviçre')}"
puts "plaka.exists?(0)         => #{plaka.exists?(0)}"
puts "plaka.exists?(33)        => #{plaka.exists?(33)}"
puts "plaka.exists?(99)        => #{plaka.exists?(99)}"

puts "plaka.has_many_names('afyon')       => #{plaka.has_many_names('afyon')}"
puts "plaka.has_many_names('istanbul')    => #{plaka.has_many_names('istanbul')}"
puts "plaka.has_many_names(33)            => #{plaka.has_many_names(33)}"
puts "plaka.has_many_names(34)            => #{plaka.has_many_names(34)}"
puts "plaka.has_many_names('33')          => #{plaka.has_many_names('33')}"
puts "plaka.has_many_names('34')          => #{plaka.has_many_names('34')}"
puts "plaka.has_many_names('isviçre')     => #{plaka.has_many_names('isviçre')}"
puts "plaka.has_many_names(0)             => #{plaka.has_many_names(0)}"
puts "plaka.has_many_names('0')           => #{plaka.has_many_names('0')}"
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

### Testing

```bash
# example usage of minitest
ruby -Ilib:test test/test_plaka.rb
```

### Building Gem

```bash
gem build plaka.gemspec
mv plaka-0.1.0.gem pkg
```

### Installing local gem

```bash
cd pkg
gem install plaka-0.1.0.gem

gem list | grep -i plaka
```

### Using gem

```ruby
require 'plaka'
plaka = Plaka::Plaka.new

plaka.show(39)
# => "kırklareli"
```

## To Do

- [ ] remove support for `turkish_support` gem

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/plaka. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/plaka/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Plaka project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/plaka/blob/main/CODE_OF_CONDUCT.md).
