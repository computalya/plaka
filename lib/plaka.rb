# frozen_string_literal: true

require 'turkish_support'
using TurkishSupport

require_relative 'plaka/plaka'
require_relative "plaka/version"

module Plaka
  class Error < StandardError; end
  # Your code goes here...
end
